// @ts-check

/**
 * @typedef {{ point:string, execName:string, from:string, res:any|undefined }} PluginParam
 * @typedef {{ isStop:boolean, isError:boolean, message:string, resultValue:string}} PluginResult
 * 
 */